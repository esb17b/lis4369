## LIS 4369 - Extensible Enterprise Solutions

## Elijah Butts

### Project 2 # Requirements:

*Sub-Heading:*

1. Complete Project 2
3. Bitbucket repo links:
   - this assignment / station locations / .pdf
4. Completion of txt and png files

#### README.md file Includes:

* Screenshot of P2 R Data Analysis 
* Link to lis4369_p2_requirements.txt file: [lis4369_p2_requirements.txt](lis4369_p2_requirements.txt) 
* Link to Plot 1 png file: [plot_disp_and_mpg_1](plot_disp_and_mpg_1.png)
* Link to Plot 2 png file: [plot_disp_and_mpg_2](plot_disp_and_mpg_2.png)
* Screenshot of Graphs

> #### Bitbucket Links:

LIS 4369: https://bitbucket.org/esb17b/lis4369/src/master/

BitbucketStationLocations: https://bitbucket.org/esb17b/bitbucketstationlocations/src/master/

#### Project 2 Graph Screenshots (R Tutorial):

| Plot 1 | Plot 2 |
| -------------------------------- | ---------------------------------- |
| ![App Screen Shot 1](img/png1.png) | ![App Screen Shot 2](img/png2.png) |

#### Project 2 Screenshots cont.:

| Txt File Screenshot | File Hiearchy |
| -------------------------------- | ---------------------------------- |
| ![App Screen Shot 1](img/txt.png) | ![App Screen Shot 2](img/files.png) |

#### Project 2 R Window Screenshots:

| R Window 1 | R Window 2 |
| -------------------------------- | ---------------------------------- |
| ![App Screen Shot 1](img/r.png) | ![App Screen Shot 2](img/r2.png) |


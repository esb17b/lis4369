# LIS 4369 - Extensible Enterprise Solutions

## Elijah Butts

### Class Number Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
   
    - Install Python
    - Install R
    - Install R Studio
    - Install Visual Studio Code
    - Create tip_calc application
    - Create tip_calc Jupyter Notebook
    - Provide screenshots of application
    - Create Bitbucket repo
    - Complete Bitbucket tutorial
    - Provide git command descriptions
    
2. [A2 README.md](a2/README.md "My A2 README.md file")

    - Develop overtime calculator
    - Use 2 different files, (functions and main)
    - Provide screenshots of Payroll Calculator
    - Provide screenshots of completed skillsets
    - Screenshots of Jupyter Notebook
    
3. [A3 README.md](a3/README.md "My A3 README.md file")
   
    - Painting Estimator 
    - Skill Sets
        - Skillset 4: Calorie Counter
        - Skill Set 5: Python Calculator
        - Skill Set 6: Loops in Python
    - Painting Estimator in Jupyter Notebook
    
4. [A4 README.md](a4/README.md "My A4 README.md file")
   
    - Data Analysis and Graphs 
    - Skill Sets
        - Skillset 10: Dictionaries 
        - Skill Set 11: Random Number Generator
        - Skill Set 12: Temerature Converter
    - Data Analysis in Jupyter Notebook
   
5. [A5 README.md](a5/README.md "My A5 README.md file")
- Completion of R Tutorial 
    - Tutorial Screenshots 
    - Skill Sets
        - Skill Set 13: Sphere Calculator 
        - Skill Set 14: Calculator
        - Skill Set 15: Read Write
    - Data Analysis in R
    - Various Graphs and Charts


### Projects:

1. [P1 README.md](p1/README.md "My P1 README.md file")

    - Project 1 Data Analysis 
    - Show printing of python generated graph
    - Panda functionality
    - Skill Sets
        - Skill Set 7: Python Lists
        - Skill Set 8: Python Tuples
        - Skill Set 9: Python Sets
    - Project 1 in Jupyter Notebook

2. [P2 README.md](p2/README.md "My P2 README.md file")
- P2 Data Analysis
    - Display png files separately 
    - Display txt file spereatley using cat commands
    - Include name in graph
    - Test using R Studio


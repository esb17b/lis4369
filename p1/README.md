## LIS 4369 - Extensible Enterprise Solutions

## Elijah Butts

### Project 1 # Requirements:

*Sub-Heading:*

1. Complete Project 1
2. Skill Sets 7 - 9 
3. Bitbucket repo links:
   - this assignment / station locations / .ipynb

#### README.md file Includes:

* Screenshot of P1 Data Analysis
* Link to P1 .ipynb file: [analysis.ipynb](p1_data_analysis_1/p1_data.ipynb) 
* Screenshot Jupyter Notebook
* Screenshots of Skillsets

> #### Bitbucket Links:

LIS 4369: https://bitbucket.org/esb17b/lis4369/src/master/

BitbucketStationLocations: https://bitbucket.org/esb17b/bitbucketstationlocations/src/master/

#### Assignment Screenshots:

| Project 1 Output              | Project 1 Output cont.         |
| -------------------------------- | ---------------------------------- |
| ![App Screen Shot 1](img/p1.png) | ![App Screen Shot 2](img/p1_2.png) |

![App Screen Shot 2.5](img/graph.png)

#### Skillset Screenshots:

| Skill Set 7: Python Lists | Skill Set 8: Python Tuples | Skill Set 9: Python Sets |
| ----------- | ----------- | ----------- |
| ![App Screen Shot 3.5](img/skill7.png) ![App Screen Shot 3.5](img/skill7_2.png) | ![App Screen Shot 4](img/skill8.png) | ![App Screen Shot 5](img/skill9.png) |


| Python demo file | Python demo file cont. |
| -------------------------------------- | -------------------------------------- |
| ![App Screen Shot 6](img/dem1.png)                        | ![App Screen Shot 7](img/dem2.png)                              |

#### Project 1 Jupyter Notebook Screenshots:

![App Screen Shot 8](img/jup1.png) 
![App Screen Shot 9](img/jup2.png)
![App Screen Shot 10](img/jup3.png)
![App Screen Shot 11](img/jup4.png)
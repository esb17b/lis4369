## LIS 4369 - Extensible Enterprise Solutions

## Elijah Butts

### Assignment 3 # Requirements:

*Sub-Heading:*

1. Painting Estimator
2. Skill Sets 4 - 6 
3. Bitbucket repo links:
   - this assignment / station locations / .ipynb

#### README.md file Includes:

* Screenshot of A3 Painting Estimator
* Link to A3 .ipynb file: [painting.ipynb](ipynb/a3_painting.ipynb) 
* Screenshot Jupyter Notebook
* Screenshots of Skillsets

> #### Bitbucket Links:

LIS 4369: https://bitbucket.org/esb17b/lis4369/src/master/

BitbucketStationLocations: https://bitbucket.org/esb17b/bitbucketstationlocations/src/master/

#### Assignment Screenshots:

| Painting First Run                      | Painting Second Run                     |
| --------------------------------------- | --------------------------------------- |
| ![App Screen Shot 1](img/painting1.png) | ![App Screen Shot 2](img/painting2.png) |


#### Skillset Screenshots:

| Skill Set 4: Calorie Counter | Skill Set 5: Valid Operator | Skill Set 5: Invalid Operator |
| ----------- | ----------- | ----------- |
| ![App Screen Shot 3](img/calorie.png) | ![App Screen Shot 4](img/calc1.png)  | ![App Screen Shot 5](img/calc2.png)  |


| Skill Set 6: Loops 1-5 | Skill Set  6:  Loops 6-10 |
| -------------------------------------- | -------------------------------------- |
| ![App Screen Shot 1](img/loop1.png)                            | ![App Screen Shot 2](img/loop2.png)                                  |

#### Jupyter Notebook Screenshots:

![App Screen Shot 6](img/jup1.png)                                 
![App Screen Shot 7](img/jup2.png)

![App Screen Shot 8](img/jup3.png)
def get_requirements():
    """Accepts 0 args, prints program requirements."""
print("Painting Estimator")
print("\nProgram Requirements:\n")
print("1. Calculate home interior paint cost (w/o primer). ")
print("2. Must use float data types.")
print("3. Must use SQFT_PER_GALLON constant (350).")
print("4. Must use iteration structure (aka loops).") 
print("5. Format, right-allign numbers, and round two decimal places.")
print("6. Create at least five functions tha care called by the program:")
print("       a. main(): calls at least two other functions.")
print("       b. get_requirements(): displays the program requirements.")
print("       c. estimate_painting_cost(): calculates interior home painting and calls print functions.")
print("       d. print_painting_estimate(): displays painting costs.")
print("       e. print_painting_percentage(): displays painting cost percentages.")


def estimate_painting_cost():
    """Accepts 0 args, calculates payroll based upon user input"""

    SQFT_PER_GALLON = 350
    total_sqft = 0.0
    

    # Obtain User Data
    print("Input:")
    total_sqft = float(input('Enter interior sqft: '))
    price_per_gallon = float(input('Enter price per gallon of paint: '))
    painting_rate = float(input('Enter hourly painting rate per sq ft: '))

    # Calculations
    paint_gallons = total_sqft / SQFT_PER_GALLON
    paint_cost = paint_gallons * price_per_gallon
    labor_cost = total_sqft * painting_rate
    total_cost = paint_cost + labor_cost
    paint_percent = paint_cost / total_cost
    labor_percent = labor_cost / total_cost

    # Calculation
    print_painting_estimate(total_sqft, SQFT_PER_GALLON, paint_gallons, 
    price_per_gallon, painting_rate)

    print_painting_percentage(paint_cost, paint_percent, labor_cost, labor_percent,
    total_cost)

def print_painting_estimate(total_sqft, SQFT_PER_GALLON, paint_gallons, price_per_gallon, 
painting_rate):
    print("\nOutput:")
    print("{0:20} {1:>9}".format("Item", "Amount"))
    print("{0:20} {1:9,.2f}".format("Total Sq Ft:", total_sqft))
    print("{0:20} {1:9,.2f}".format("Sq Ft per Gallon:", SQFT_PER_GALLON))
    print("{0:20} {1:9,.2f}".format("Number of Gallons:", paint_gallons))
    print("{0:20} ${1:8,.2f}".format("Paint per Gallon:", price_per_gallon))
    print("{0:20} ${1:8,.2f}".format("Labor per Sq Ft:", painting_rate))
    
    print()

def print_painting_percentage(paint_cost, paint_percent, labor_cost, 
labor_percent, total_cost):

    print("{0:8} {1:>9} {2:>13}".format("Cost", "Amount", "Percentage" ))

    print("{0:8} ${1:8,.2f} {2:13.2%}".format("Paint:", paint_cost, paint_percent))

    print("{0:8} ${1:8,.2f} {2:13.2%}".format("Labor:", labor_cost, labor_percent))

    print("{0:8} ${1:8,.2f} {2:13.2%}".format("Total:", total_cost, paint_percent + labor_percent))
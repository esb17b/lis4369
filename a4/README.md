## LIS 4369 - Extensible Enterprise Solutions

## Elijah Butts

### Assignment 4 # Requirements:

*Sub-Heading:*

1. Complete Assignment 4
2. Skill Sets 10 - 12 
3. Bitbucket repo links:
   - this assignment / station locations / .ipynb

#### README.md file Includes:

* Screenshot of A4 Data Analysis 2
* Link to A4 .ipynb file: [analysis.ipynb](a4_data_analysis_2/a4_data.ipynb) 
* Screenshot Jupyter Notebook
* Screenshots of Skillsets

> #### Bitbucket Links:

LIS 4369: https://bitbucket.org/esb17b/lis4369/src/master/

BitbucketStationLocations: https://bitbucket.org/esb17b/bitbucketstationlocations/src/master/

#### Assignment Screenshots:

| Assignment 4 Output  | Assignment 4 Output cont. |
| -------------------------------- | ---------------------------------- |
| ![App Screen Shot 1](img/a4.png) | ![App Screen Shot 2](img/a4_2.png) |

![App Screen Shot 2.5](img/graph.png)

#### Skillset Screenshots:

| Skill Set 10: Dictionaries | Skill Set 11: Random Number Generator | Skill Set 12: Temperature |
| ----------- | ----------- | ----------- |
| ![App Screen Shot 3.5](img/skill10.png) | ![App Screen Shot 4](img/skill11.png) | ![App Screen Shot 5](img/skill12.png) |


#### Assignment 4 Jupyter Notebook Screenshots:

![App Screen Shot 6](img/jup1.png) 
![App Screen Shot 7](img/jup2.png)
![App Screen Shot 8](img/jup3.png)
![App Screen Shot 9](img/jup4.png)
![App Screen Shot 10](img/jup5.png)
![App Screen Shot 11](img/jup6.png)
def get_requirements():
    """Accepts 0 args, prints program requirements."""
print("Payroll Calculator (***Note:*** module and function docstrings above.)")
print("\nProgram Requirements:\n")
print("1. Must use float dta type for the user input.")
print("2. Overtime rate: 1.5 times hourly rate (hours over 40).")
print("3. Holiday rate: 2.0 times hourly rate (all holiday hours).")
print("4. Must format currency with dollar sign, and round to two decimal places.")
print("5. Create at least three functions that are called by the program.")
print("       a. main(): calls at least two other functions.")
print("       b. get_requirements(): displays the program requirements.")
print("       c. calculate_payroll(): calculates an individual one-week paycheck.")

def calculate_payroll():
    """Accepts 0 args, calculates payroll based upon user input"""
    BASE_HOURS = 40
    OT_RATE = 1.5
    HOLIDAY_RATE = 2.0

    # Obtain User Data
    print("Input:")
    # Get Hours Worked
    hours = float(input('Enter hours worked: '))
    holiday_hours = float(input('Enter holiday hours worked: '))
    pay_rate = float(input('Enter hourly pay rate: '))

    # Calculations
    base_pay = BASE_HOURS * pay_rate
    overtime_hours = hours - BASE_HOURS

    # Calculation
    if hours > BASE_HOURS:
        overtime_pay = overtime_hours * pay_rate * OT_RATE
        holiday_pay = holiday_hours * pay_rate * HOLIDAY_RATE
        gross_pay = BASE_HOURS * pay_rate + overtime_pay + holiday_pay
        print_pay(base_pay, overtime_pay, holiday_pay, gross_pay)
    else:
        overtime_pay = 0
        holiday_pay = holiday_hours * pay_rate * HOLIDAY_RATE
        gross_pay = hours * pay_rate + holiday_pay

        print_pay(base_pay, overtime_pay, holiday_pay, gross_pay)

        print()

def print_pay(base_pay,overtime_pay,holiday_pay,gross_pay):
    """Accepts 4 args, formats and prints payroll variables."""
    print("\nOutput:")
    print("{0:<10} ${1:,.2f}".format('Base:', base_pay))
    print("{0:<10} ${1:,.2f}".format('Overtime:', overtime_pay))
    print("{0:<10} ${1:,.2f}".format('Holiday:', holiday_pay))
    print("{0:<10} ${1:,.2f}".format('Gross:', gross_pay))

## LIS 4369 - Extensible Enterprise Solutions

## Elijah Butts

### Assignment 2 # Requirements:

*Sub-Heading:*

1. Payroll Calculator
2. Skill Sets 1 - 3 
3. Bitbucket repo links:
   - this assignment / station locations / .ipynb

#### README.md file Includes:

* Screenshot of A2 Payroll Calculator running
* Link to A2 .ipynb file: [payroll.ipynb](ipynb/a2_payroll.ipynb) 
* Screenshot Jupyter
* Screenshots of Skillsets

> #### Bitbucket Links:

LIS 4369: https://bitbucket.org/esb17b/lis4369/src/master/

BitbucketStationLocations: https://bitbucket.org/esb17b/bitbucketstationlocations/src/master/


#### Assignment Screenshots:

| Payroll With No Overtime | Payroll With Overtime |
| -------------------------------------- | -------------------------------------- |
| ![App Screen Shot 1](img/Payroll1.png)                                 | ![App Screen Shot 2](img/Payroll2.png)                                        |

| Skill Set 1: Square Feet to Acres      | Skill Set 2: Miles Per Gallon | Skill Set 3: IT/ICT Percentage |
| ----------- | ----------- | ----------- |
| ![App Screen Shot 3](img/Acres.png)      | ![App Screen Shot 4](img/Miles.png)       | ![App Screen Shot 5](img/IT.png)       |

#### Jupyter Notebook Screenshots:

![App Screen Shot 6](img/Jup1.png)                                 
![App Screen Shot 7](img/Jup2.png)

![App Screen Shot 8](img/Jup3.png)
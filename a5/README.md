## LIS 4369 - Extensible Enterprise Solutions

## Elijah Butts

### Assignment 5 # Requirements:

*Sub-Heading:*

1. Complete Assignment 5
2. Skill Sets 13 - 15 
3. Bitbucket repo links:
   - this assignment / station locations / .pdf
4. Completion of R tutorial

#### README.md file Includes:

* Screenshot of A5 R Data Analysis 
* Link to A5 pdf file: [myplotfile.pdf](r_tutorial/myplotfile.pdf) 
* Link to A5 tutorial R file: [learn_to_use_r.R](r_tutorial/learn_to_use_r.R)
* Screenshot of Graphs
* Screenshots of Skillsets

> #### Bitbucket Links:

LIS 4369: https://bitbucket.org/esb17b/lis4369/src/master/

BitbucketStationLocations: https://bitbucket.org/esb17b/bitbucketstationlocations/src/master/

#### Assignment Screenshots (R Tutorial):

| Assignment 5 Tutorial  | Assignment 5 Tutorial cont. |
| -------------------------------- | ---------------------------------- |
| ![App Screen Shot 1](img/tut1.png) | ![App Screen Shot 2](img/tut2.png) |

#### Assignment Screenshots cont. (R Tutorial):

| Assignment 5 Tutorial  | Assignment 5 Tutorial cont. |
| -------------------------------- | ---------------------------------- |
| ![App Screen Shot 1](img/graph1.png) | ![App Screen Shot 2](img/graph2.png) |

#### Skillset Screenshots:

| Skill Set 13: Sphere Gallons | Skill Set 14: Calculator | Skill Set 15: Read Write |
| ----------- | ----------- | ----------- |
| ![App Screen Shot 3.5](img/skill13.png) | ![App Screen Shot 4](img/skill14.png) | ![App Screen Shot 5](img/skill15.png) |


#### Assignment 5 Titanic Info Screenshots:

![App Screen Shot 6](img/titanic1.png) 

| Assignment 5 Titanic  | Assignment 5 Titanic cont. |
| -------------------------------- | ---------------------------------- |
| ![App Screen Shot 1](img/titanic2.png) | ![App Screen Shot 2](img/titanic3.png) |

#### Assignment 5 Titanic Graphs Screenshots:

| Assignment 5 Titanic  | Assignment 5 Titanic cont. |
| -------------------------------- | ---------------------------------- |
| ![App Screen Shot 1](img/graph3.png) | ![App Screen Shot 2](img/graph4.png) |

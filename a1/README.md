## LIS 4369 - Extensible Enterprise Solutions

## Elijah Butts

### Assignment 1 # Requirements:

*Sub-Heading:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installation
3. Questions
4. Bitbucket repo links:
   a)  this assignment / completed tutorial

#### README.md file Includes:

* Screenshot of A1 tip calc running
* Link to A1 .ipynb file: [tip_calc.ipynb](ipynb/tip_calc.ipynb) 
* Screenshot Jupyter
* Git commands with description

> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git-config - Get and set repository or global options

> #### Bitbucket Links:

LIS 4369: https://bitbucket.org/esb17b/lis4369/src/master/

BitbucketStationLocations: https://bitbucket.org/esb17b/bitbucketstationlocations/src/master/


#### Assignment Screenshots:

*Screenshot of APP running*:
![App Screen Shot 1](img/FirstScreen.png)

*Screenshot of tip_calc running through VS studio*:

![App Screen Shot 2](img/SecondScreen.png)

*Screenshot of tip_calc running through IDLE*:

![Java Hello Screen Shot](img/Third.png)

*Screenshot of Jupyter Notebook*:

![Android Development Studio](img/Fourth.png)


